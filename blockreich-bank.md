Blockreich-Bank ist das Banksystem Blockreichs.

---

## Abhängigkeiten

* [Blockreich-Economy](blockreich-economy.md)
* Vault

## Funktion
* Verwaltet und speichert die Bankaccounts der User in einer Datenbank *(MySql oder SQLite)*
* Eigene API um von der Vaultapi nicht bereitgestellte Aktionen ausführen zu können *(Konto erstellen und löschen)*
* Transaktionsverlauf und “Kontoauszüge”


