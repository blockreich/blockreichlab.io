Dokumentationen der **Blockreich** Minecraft-Server Plugins.


## Inhalt

* [Blockreich Bank](blockreich-bank.md)
* [Blockreich Economy](blockreich_economy.md)
* [Blockreich Utils](blockreichutils.md)